import React, { Component } from 'react';
import './App.css';

import Menu from "./screens/Menu.js"

export default class App extends Component {
  render() {
    const { children } = this.props;
    return (
      <div>
        <Menu routes={this.props.children} />
      </div >
    )
  }
}
