import React from 'react'

const TemperatureLogTable = props => (
    <tr>
        <td>{props.data}</td>
        <td>{Number(props.c).toFixed(2)}</td>
        <td>{Number(props.c / 5 * 9 + 32).toFixed(2)}</td>
    </tr>
)

export default TemperatureLogTable