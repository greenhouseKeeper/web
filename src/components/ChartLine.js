import React, { Component } from 'react';
import { Line } from 'react-chartjs-2';

class Chart extends Component {
    constructor(props) {
        super(props)

        this.state = {
            opa: 100,
            chartData: this.props.chartData,
        }
    }

    componentDidMount = () => {

    }

    render() {
        return (
            <div className="chart">
                <Line
                    data={this.state.chartData}
                    width={100}
                    heigth={50}
                    options={{
                        maintainAspectRatio: false
                    }}
                />
            </div>
        )
    }
}

export default Chart;