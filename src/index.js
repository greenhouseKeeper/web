import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { BrowserRouter as Router, Route } from "react-router-dom"


import Main from "./screens/Main"
import Statistics from './screens/Statistics'
import Configurations from './screens/Configurations'

ReactDOM.render(
    <Router>
        <App>
            <Route path='/' exact component={Main} />
            <Route path='/Main' component={Main} />
            <Route path='/Statistics' component={Statistics} />
            <Route path='/Configurations' component={Configurations} />
        </App>
    </Router>
    , document.getElementById('root'));