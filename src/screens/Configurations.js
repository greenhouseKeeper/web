import React, { Component } from 'react'
import axios from "axios"

class Configurations extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: '',
            min: '',
            max: '',
            config: null,
            aviso: '',
        }
    }

    componentDidMount = () => {
        this.getConfig()
    }

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value })
    }

    handleSubmit = e => {
        e.preventDefault()

        axios({
            method: 'post',
            url: 'https://cors-anywhere.herokuapp.com/https://frozen-bayou-51290.herokuapp.com/api/config',
            data: {
                email: this.state.email,
                min: this.state.min,
                max: this.state.max,
            },
            validateStatus: (status) => {
                return true; // I'm always returning true, you may want to do it depending on the status received
            },
        }).catch(error => {

        }).then(response => {
            this.setState({ aviso: 'Configurações salvas com sucesso.' })
            this.tempoAviso()
        });
    }

    getConfig = () => {
        axios.get('https://cors-anywhere.herokuapp.com/https://frozen-bayou-51290.herokuapp.com/api/config')
            .then(log => {
                const data = log.data
                this.setState({ config: data })
                this.setState({
                    email: log.data.email[0].evento,
                    min: log.data.min[0].evento,
                    max: log.data.max[0].evento
                })
            });
    }

    tempoAviso = () => {
        setTimeout(() => {
            this.setState({ aviso: '' })
        }, 5000)
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm-2"></div>
                    <div className="col-sm-8">

                        <form onSubmit={this.handleSubmit}>
                            <div className="input-group mt-3">
                                <div className="input-group-prepend">
                                    <span className="input-group-text">
                                        Email
                                    </span>
                                </div>
                                <input type="email" className="form-control"
                                    placeholder="Email para Comunicação"
                                    name="email"
                                    onChange={this.handleChange}
                                    value={this.state.email}
                                />
                            </div>
                            <div className="input-group mt-3">
                                <div className="input-group-prepend">
                                    <span className="input-group-text">
                                        Temperatura Mínima
                                    </span>
                                </div>
                                <input type="value" className="form-control"
                                    placeholder="Temperatura Mínima"
                                    name="min"
                                    onChange={this.handleChange}
                                    value={this.state.min}
                                />
                            </div>
                            <div className="input-group mt-3">
                                <div className="input-group-prepend">
                                    <span className="input-group-text">
                                        Temperatura Máxima
                                    </span>
                                </div>
                                <input type="value" className="form-control"
                                    placeholder="Temperatura Máxima"
                                    name="max"
                                    onChange={this.handleChange}
                                    value={this.state.max}
                                />
                            </div>
                            <input type="submit" className="btn btn-danger float-right mt-3"
                                value="Salvar" />
                        </form>
                        <div style={{ marginTop: '15vh' }}>
                            {this.state.aviso !== '' ?
                                <div className='alert alert-info mt-3'>
                                    {this.state.aviso}
                                </div>
                                : ''
                            }
                        </div>
                    </div>
                    <div className="col-sm-2"></div>
                </div>
            </div>
        )
    }
}

export default Configurations
