import React, { Component } from 'react'
import axios from 'axios'

import TemperatureLogTable from '../components/TemperatureLogTable'
import Chart from '../components/Chart'

class Main extends Component {

    state = {
        temperatureLog: [],
        chartData: {
            labels: ['14:20:00', '14:10:00', '14:00:00'],
            datasets: [
                {
                    label: 'Temperaturas',
                    data: [
                        22.4,
                        23.5,
                        22.2,
                    ],
                    backgroundColor: [
                        'rgba(255,99,132,0.6)',
                        'rgba(54,162,235,0.6)',
                        'rgba(75,192,192,0.6)'
                    ]
                }
            ]
        },
    }

    componentDidMount = () => {
        setInterval(this.getTemperatureLog, 3000)
        // this.getTemperatureLog()
    }

    getTemperatureLog = () => {
        let temperatureLog = [{}]
        axios.get('https://cors-anywhere.herokuapp.com/https://frozen-bayou-51290.herokuapp.com/api/temperature_min')
            .then(log => {
                const data = log.data
                this.setState({ temperatureLog: data })
            });
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm-6">
                        <h5> Gráfico </h5>
                        <Chart chartData={this.state.chartData} />
                    </div>
                    <div className="col-sm-6">
                        <h5> Tabela </h5>
                        <table className="table table-sm table-striped table-bordered table-action mx-2">
                            <thead>
                                <tr className="table-primary">
                                    <th>Hora</th>
                                    <th>C°</th>
                                    <th>F°</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.temperatureLog.map((log, num) => (
                                    <TemperatureLogTable
                                        key={log.num}
                                        data={log.updated_at}
                                        c={log.celsius_temperature}
                                    />
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

export default Main
