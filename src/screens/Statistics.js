import React, { Component } from 'react'
import axios from 'axios'
import jsPDF from 'jspdf'
import 'jspdf-autotable';
import Chart from '../components/ChartLine'

class Statistics extends Component {
    constructor(props) {
        super(props)

        this.state = {
            temperatureLog: null,
            media: 0,
            chartData: {
                labels: ['14:20:00', '14:10:00', '14:00:00', '13:50:00', '13:40:00',
                    '13:30:00', '13:20:00', '13:10:00', '13:00:00'],
                datasets: [
                    {
                        label: 'Temperaturas',
                        data: [
                            22.4,
                            23.5,
                            22.2,
                            22.4,
                            22.6,
                            22.8,
                            22.6,
                            21.9,
                            21.0,
                        ],
                        backgroundColor: [
                            'rgba(255,99,132,0.6)',
                            'rgba(54,162,235,0.6)',
                            'rgba(75,192,192,0.6)',
                            'rgba(75,192,192,0.6)',
                            'rgba(75,192,192,0.6)',
                            'rgba(75,192,192,0.6)',
                            'rgba(75,192,192,0.6)',
                            'rgba(75,192,192,0.6)',
                            'rgba(75,192,192,0.6)',
                        ]
                    }
                ]
            },
        }
    }

    componentDidMount = () => {
        this.getTemperatureLog()
    }

    getTemperatureLog = () => {
        let temperatureLog = [{}]
        axios.get('https://cors-anywhere.herokuapp.com/https://frozen-bayou-51290.herokuapp.com/api/temperature')
            .then(log => {
                const data = log.data
                this.setState({ temperatureLog: data })
                this.analysis()
            });
    }

    async analysis() {
        let media = 0;
        let temperatureLog = this.state.temperatureLog;
        temperatureLog.map((data) => {
            media += Number(data.celsius_temperature);
        })
        media = Number(media / temperatureLog.length).toFixed(2)
        this.setState({ media })
    }

    report = () => {
        let doc = new jsPDF()
        doc.setFontSize(22)
        doc.text('Relatório da Semana', 72, 12)
        doc.setFontSize(16)
        doc.text('GreenHouse Keeper', 85, 20)
        doc.setFontSize(12)
        let date = new Date
        let data = ("00" + date.getDate()).slice(-2) +
            "/" + ("00" + Number(date.getMonth() + 1)).slice(-2) +
            "/" + date.getFullYear()
        let hora = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds()
        doc.text('Data: ' + data + '  Hora: ' + hora, 72, 27)
        doc.text('Média: Cº ' + this.state.media, 20, 35)
        doc.autoTable({
            head: [
                {
                    updated_at: 'Data',
                    celsius_temperature: 'Cº',
                }
            ],
            body: this.state.temperatureLog,
            startY: 40,
            /* apply styling to table body */
            bodyStyles: {
                valign: 'top'
            },
            /* apply global styling */
            styles: {
                cellWidth: 'wrap',
                rowPageBreak: 'auto',
                halign: 'justify',
            },
            /* apply styling specific to table columns */
            columnStyles: {
                text: {
                    cellWidth: 'auto'
                }
            }
        });
        doc.output('dataurlnewwindow', "GreenHouse Keeper.pdf")
    }

    render() {
        return (
            <div>
                <h3>Tela de Estatisticas</h3>
                <input type="button" value="Relatório" class="btn btn-success" onClick={this.report} />
                <div className="container">
                    <div className="row">
                        <div className="col-sm-2"></div>
                        <div className="col-sm-8">
                            <Chart chartData={this.state.chartData} />
                        </div>
                        <div className="col-sm-2"></div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Statistics
