import React, { Component } from 'react'
import Moment from 'moment'
import axios from 'axios'

import { Link } from 'react-router-dom'

import '.././styles/Menu.css'

import userIcon from '../icons/userIcon.png'
import plantTemperatureIcon from '../icons/plantTemperature.png'
import diagramIcon from '../icons/diagram.png'
import thermometerIcon from '../icons/thermometer.png'
import settingsIcon from '../icons/settings.png'

export default class Header extends Component {
    constructor(props) {
        super(props)

        this.state = {
            time: '',
            date: '',
            title: 'Principal',
            temperatureLog: [],
            actualTemp: null,
            lastTemp: null,
        }
    }

    componentDidMount = () => {
        this.getTemperatureLog()
        setInterval(this.getTime, 3000)
        setInterval(this.getActualTemperature, 1000)
        setInterval(this.getTemperatureLog, 5000)
        setInterval(this.checkConnection, 15500)
    }

    checkConnection = () => {
        let data = this.state.temperatureLog[0]
        let last = this.state.lastTemp
        if (data) {
            data = data.updated_at
            if (last) {
                if (last === data) {
                    this.sendTemperatureConnectionFailed()
                }
            } else {
                this.setState({ lastTemp: data })
            }
        }
    }

    sendTemperatureConnectionFailed() {
        axios({
            method: 'post',
            url: 'https://cors-anywhere.herokuapp.com/https://frozen-bayou-51290.herokuapp.com/api/connection',
            data: {
                teste: 'teste',
            },
            validateStatus: (status) => {
                return true; // I'm always returning true, you may want to do it depending on the status received
            },
        })
    }

    getTemperatureLog = () => {
        let temp = []
        axios.get('https://cors-anywhere.herokuapp.com/https://frozen-bayou-51290.herokuapp.com/api/temperature_min')
            .then(function (response) {
                response.data.map(d => {
                    temp.push({
                        celsius_temperature: d.celsius_temperature,
                        created_at: d.created_at,
                        id: d.id,
                        updated_at: d.updated_at
                    })
                })
            });
        this.setState({ temperatureLog: temp })
    }

    getTime = () => {
        Moment.locale('pt-br')
        let time = Moment().format('LT')
        let date = Moment().format('DD/MM/YY')
        this.setState({ time, date })
    }

    getActualTemperature = () => {
        if (this.state.temperatureLog.length > 0) {
            this.setState({ actualTemp: this.state.temperatureLog[0].celsius_temperature })
        }
    }


    getTitle = (title) => {
        this.setState({ title })
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm-3 sideBar" style={{ backgroundColor: '#bfff98', height: '100vh' }}>
                        <img src={userIcon} alt="" style={{ width: '23vw', height: '23vw' }} />
                        <h4 style={{ textAlign: 'center' }}>João Almeida</h4>
                        <div className="row">
                            <div className="col-sm-4">
                                <img src={plantTemperatureIcon} alt="icone" style={{ width: '6vw' }} />
                            </div>
                            <div className="col-sm-8" style={{ textAlign: 'center' }}>
                                <h2>Cº {this.state.actualTemp}</h2>
                            </div>
                        </div>
                        <div className="row" style={{ color: 'green' }}>
                            <div className="col-sm-12 bottomText" >
                                <h6>
                                    <b>Floricultura Jasmin</b>
                                </h6> <br />
                                <h6>GreenHouse Keeper v.0.0</h6>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-9">
                        <div className="row mTop">
                            <div className="col-sm-8 topBarMenu">
                                <div className="SmallActualTemperature" style={{}}>
                                    <h2>Cº {this.state.actualTemp}</h2>
                                </div>
                                <Link to={'/Main'} onClick={() => { this.getTitle('Principal') }}>
                                    <img src={thermometerIcon} alt="" />
                                </Link>
                                <Link to={'/Statistics'} onClick={() => { this.getTitle('Estatísticas') }}>
                                    <img src={diagramIcon} alt="" />
                                </Link>
                                <Link to={'/Configurations'} onClick={() => { this.getTitle('Configurações') }}>
                                    <img src={settingsIcon} alt="" />
                                </Link>
                                {/* className="topBarMenu" */}
                            </div>
                            <div className="col-sm-4">
                                <h4>{this.state.time}</h4> <br />
                                <h4>{this.state.date}</h4> <br />
                                <h1>
                                    {this.state.title}
                                </h1>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-sm-12">
                                {this.props.routes}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
