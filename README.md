## GreenHouse Keeper - Front-end React.js

### Introdução

A empresa Floricultura Jasmin está investindo na construção de uma estufa para
fazer o crescimento de suas plantas e flores. O cliente necessita de informações
da temperatura do ambiente via software, assim como relatórios das variações de
temperatura na estufa para prevenir temperaturas inadequadas e um possível
estrago em seu plantio.

### Descrição técnica
(Descrição técnica do projeto geral na wiki)

 O Front-end Web do projeto foi projetado em React.js. O lado do cliente cuida de
 requisitar as informações de temperatura com sua hora para a api, gerar relatório,
 cuidar de requisições periódicas para atualização de informações e fornecer um menu
 de configuração.
 
### Wiki

[https://gitlab.com/greenhouseKeeper/api/wikis/pages](https://gitlab.com/greenhouseKeeper/api/wikis/pages)
 
### Linguagens

*  Javascript
*  HTML
*  Css
 
### Tecnologias
 
 *  React.js
 *  Bootstrap 4
 *  Axios
 *  JsPDF
 *  Chart.js (até o momento meramente ilustrativos)
 *  Moment.js

### Requisitos:

*  Git => [https://git-scm.com/downloads]()
*  npm => [https://www.npmjs.com/get-npm]()

### Como executar:

*  Faça o clone do repositório
*  Dentro da pasta 'web', execute o comando: npm i
*  Agora, apenas executar: npm start
 Seu servidor local agora está sendo executado, basta abrir em "localhost:3000"

### Erros possíveis:

Na execução pode ocorrer o seguinte erro ao executar o projeto (npm start):

`npm ERR! code ELIFECYCLE`

`npm ERR! errno 1`

`npm ERR! greenhouse_front@0.1.0 start: ` `react-scripts start`

`npm ERR! Exit status 1`

Provavelmente você está utilizando Linux, rode o comando com permissão de super
usuário "sudo npm start".


